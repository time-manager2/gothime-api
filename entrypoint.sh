#!/bin/sh

export PGPASSWORD=$POSTGRES_PASSWORD

while ! pg_isready -q -h $POSTGRES_HOST -p 5432 -U $POSTGRES_USER
do
  echo "Waiting for database to start"
  sleep 2
done

mix ecto.setup

psql -h $POSTGRES_HOST -U $POSTGRES_USER -d $POSTGRES_DB -a -f init.sql

mix phx.server
