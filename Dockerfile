FROM elixir

RUN apt-get update -y && apt-get -y install postgresql-client

ADD . /app

RUN mix local.hex --force && mix local.rebar --force

WORKDIR /app

RUN mix setup

CMD ["./entrypoint.sh"]
