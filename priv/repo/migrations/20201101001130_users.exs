defmodule Api.Repo.Migrations.Users do
  use Ecto.Migration

  def up do
    create table(:users) do
      add :username, :string, null: false
      add :email, :string, null: false
      add :password_hash, :string, null: false

      timestamps()
    end
    create unique_index(:users, [:email])
  end

  def down do
    drop table(:users)
  end
end
