defmodule Api.Repo.Migrations.RolesPermissions do
  use Ecto.Migration

  def up do
    create table(:roles_permissions) do
      add :role_id, references(:roles, on_delete: :delete_all), null: false
      add :permission_id, references(:permissions, on_delete: :delete_all), null: false

      timestamps()
    end
    create unique_index(:roles_permissions, [:role_id, :permission_id])
  end

  def down do
    drop table(:roles_permissions)
  end
end
