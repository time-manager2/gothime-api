defmodule Api.Repo.Migrations.Workingtimes do
  use Ecto.Migration

  def up do
    create table(:workingtimes) do
      add :start, :naive_datetime, null: false
      add :end, :naive_datetime, null: false
      add :user, :integer, null: false
      timestamps()
    end
  end

  def down do
    drop table(:workingtimes)
  end
end
