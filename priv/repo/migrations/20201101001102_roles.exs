defmodule Api.Repo.Migrations.Roles do
  use Ecto.Migration

  def up do
    create table(:roles) do
      add :name, :string, null: false
      add :display_name, :string, null: false

      timestamps()
    end
    create unique_index(:roles, [:name])
  end

  def down do
    drop table(:roles)
  end
end
