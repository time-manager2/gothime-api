defmodule Api.Repo.Migrations.Permissions do
  use Ecto.Migration

  def up do
    create table(:permissions) do
      add :name, :string, null: false
      add :display_name, :string, null: false

      timestamps()
    end
    create index(:permissions, [:name], unique: true)
  end

  def down do
    drop table(:permissions)
  end
end
