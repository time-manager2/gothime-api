INSERT INTO public.permissions(
	name, display_name, inserted_at, updated_at)
	VALUES
		('user.manage', 'Create, view, delete and edit users', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
	ON CONFLICT DO NOTHING;

INSERT INTO public.roles(
	name, display_name, inserted_at, updated_at)
	VALUES
		('general', 'General manager', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
		('manager', 'Manager', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
		('employee', 'Employee', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
	ON CONFLICT DO NOTHING;

INSERT INTO public.roles_permissions(
	role_id, permission_id, inserted_at, updated_at)
	VALUES
	(
		(SELECT id FROM public.roles WHERE name = 'general'),
		(SELECT id FROM public.permissions WHERE name = 'user.manage'),
		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
	)
	ON CONFLICT DO NOTHING;

INSERT INTO public.users(
	username, email, password_hash, inserted_at, updated_at)
	VALUES ('Bruce Wayne', 'bruce.wayne@gothime.fr', '$2b$12$TW4Z589aRftIJXCpssABYenZ64Citah2hali9BvV.rUrPiQnzV4VO', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
	ON CONFLICT DO NOTHING;

INSERT INTO public.users_roles(
	user_id, role_id, inserted_at, updated_at)
	VALUES
	(
		(SELECT id FROM public.users WHERE email = 'bruce.wayne@gothime.fr'),
		(SELECT id FROM public.roles WHERE name = 'general'),
		CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
	)
	ON CONFLICT DO NOTHING;
