defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug CORSPlug, origin: ["*"]
    plug :accepts, ["json"]
  end

  pipeline :user_manage do
    plug ApiWeb.Plugs.JWT, permissions: ["user.manage"]
  end

  pipeline :user_logged do
    plug ApiWeb.Plugs.JWT
  end

  scope "/api/role", ApiWeb do
    pipe_through :api

    get "/", RoleController, :get_roles
  end

  scope "/api/user", ApiWeb do
    pipe_through :api

    get "/login", UserController, :login
  end

  scope "/api/user", ApiWeb do
    pipe_through :api
    pipe_through :user_logged

    get "/me", UserController, :me
  end

  scope "/api/user", ApiWeb do
    pipe_through :api
    pipe_through :user_manage

    # Get All users by role
    get "/employees", UserController, :getEmployees
    get "/managers", UserController, :getManagers

    # Crud
    get "/:userId", UserController, :getById
    post "/", UserController, :create
    put "/:userId", UserController, :update
    delete "/:userId", UserController, :destroy

    # Role
    put "/:userId/role/:roleId", RoleController, :assign_to_user
  end

  scope "/api/workingtimes", ApiWeb do
    pipe_through :api

    get "/:userId", WorkingTimeController, :getByUserId
    get "/:userId/:workingtimeId", WorkingTimeController, :getByWorkingTimeId
    post "/:userId", WorkingTimeController, :create
    put "/:id", WorkingTimeController, :update
    delete "/:id", WorkingTimeController, :delete
  end

  scope "/api/teams", ApiWeb do
    pipe_through :api

    get "/:id", TeamController, :getById
    get "/:id/workingtimes", TeamController, :getByPeriod
    post "/:id", TeamController, :create
    delete "/:id", TeamController, :delete
  end

  scope "/api/clocks", ApiWeb do
    pipe_through :api

    get "/:userId", ClockController, :getByUserId
    post "/:userId", ClockController, :create
  end

  def swagger_info do
  %{
    info: %{
      version: "1.0",
      title: "Gothime API",
      basePath: "/api",
      host: "localhost:4000"
    }
  }
  end
end
