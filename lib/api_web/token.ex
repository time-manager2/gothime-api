defmodule ApiWeb.Token do
  use Joken.Config

  def token_config, do: default_claims(default_exp: 3600 * 24 * 14) # 14 days
end
