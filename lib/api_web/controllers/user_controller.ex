defmodule ApiWeb.UserController do
  use ApiWeb, :controller
  use PhoenixSwagger

  swagger_path :create do
    post "/user"
    summary "Create a user"
    description "Create a user"
    parameter :query, :username, :string, "username", required: true
    parameter :query, :email, :string, "email", required: true
    parameter :query, :password, :string, "user's password", required: true
    response 200, "", Schema.ref(:Users)
    tag "users"
  end

  def create(conn, params) do
    if Map.has_key?(params, "password") === false do
        json(conn, %{
            content: %{
                password: [
                    "can't be blank"
                ]
            },
            error: "Invalid object"
        })
    end
    params = Map.put_new(params, "password_hash", Bcrypt.hash_pwd_salt(params["password"]))
    params = Map.delete(params, "password")
    user = User.changeset(%User{roles: []}, params)
    case Api.Repo.insert(user) do
        {:ok, user} ->
          user = if Map.has_key?(params, "roles") do
            if params["roles"] != nil do
              if Enum.empty?(params["roles"]) == false do
                users = Enum.map(params["roles"], fn role ->
                  dbRole = Role.get_by_name(role)
                  case dbRole do
                    nil -> user
                    dbRole -> User.add_role(user, dbRole)
                  end
                end)
                List.first(users)
              else
                user
              end
            else
              user
            end
          else
            user
          end
          json conn, user
        {:error, changeset} ->
            conn
            |> put_status(:not_acceptable)
            |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end

  swagger_path :getEmployees do
    get "/user/employees"
    summary "Get all known employees"
    description "Get all known employees. Reachable by general manager and managers"
    response 200, "", Schema.ref(:Users)
    tag "users"
  end

  def getEmployees(conn, _) do
    employees = User.get_employees()
    json conn, employees
  end

  swagger_path :getManagers do
    get "/user/employees"
    summary "Get all known employees"
    description "Get all known employees. Reachable by general manager and managers"
    response 200, "", Schema.ref(:Users)
    tag "users"
  end

  def getManagers(conn, _) do
    managers = User.get_managers()
    json conn, managers
  end

  swagger_path :getById do
    get "/users/{userId}"
    summary "Get user"
    description "Get a user, filtering by ID"
    parameter :query, :id, :integer, "user's id", required: true
    response 200, "", Schema.ref(:Users)
    tag "users"
  end

  def me(conn, _) do
    json conn, conn.assigns.user_logged
  end

  def getById(conn, %{"userId" => userId}) do
      userId = case Integer.parse(userId) do
          {parsed, ""} -> parsed
          _ ->
              conn
              |> put_status(:not_acceptable)
              |> json(%{error: "Invalid userId"})
      end
      case User
            |> Api.Repo.get(userId)
            |> Api.Repo.preload([:roles, {:roles, :permissions}])
        do
        nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "User not found"})
        user -> json conn, user
      end
  end

  swagger_path :update do
    put "/users"
    summary "Update a user"
    description "Update a user"
    parameter :query, :id, :integer, "users's id", required: true
    parameter :query, :username, :string, "new username", required: true
    parameter :query, :email, :string, "new email", required: true
    parameter :query, :password, :string, "user's password", required: true
    response 200, "", Schema.ref(:Users)
    tag "users"
  end

  def update(conn, params) do
      userId = case Integer.parse(params["userId"]) do
          {parsed, ""} -> parsed
          _ ->
              conn
              |> put_status(:not_acceptable)
              |> json(%{error: "Invalid userId"})
      end
      user = case User
                |> Api.Repo.get(userId)
                |> Api.Repo.preload([:roles, {:roles, :permissions}])
            do
                nil ->
              conn
              |> put_status(:not_found)
              |> json(%{error: "User not found"})
          user -> user
      end
      params = case Map.has_key?(params, "password") do
        true ->
          params = Map.put_new(params, "password_hash", Bcrypt.hash_pwd_salt(params["password"]))
          Map.delete(params, "password")
        false -> params
      end
      user = User.changeset(user, params)
      case Api.Repo.update(user) do
          {:ok, user} ->
            user = if Map.has_key?(params, "roles") do
              if params["roles"] != nil do
                if Enum.empty?(params["roles"]) == false do
                  users = Enum.map(params["roles"], fn role ->
                    dbRole = Role.get_by_name(role)
                    case dbRole do
                      nil -> user
                      dbRole -> User.add_role(user, dbRole)
                    end
                  end)
                  List.first(users)
                else
                  user
                end
              else
                user
              end
            else
              user
            end
            json conn, user
          {:error, changeset} ->
              conn
              |> put_status(:not_acceptable)
              |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
      end
  end

  swagger_path :destroy do
    delete "/user/{userId}"
    summary "Delete a user"
    description "Delete a user, filtering by user ID"
    parameter :query, :id, :integer, "user ID", required: true
    response 200, "", Schema.ref(:Users)
    tag "users"
  end

  def destroy(conn, %{"userId" => userId}) do
    userId = case Integer.parse(userId) do
        {parsed, ""} -> parsed
        _ ->
            conn
            |> put_status(:not_acceptable)
            |> json(%{error: "Invalid userId"})
    end
    user = case Api.Repo.get(User, userId) do
        nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "User not found"})
        user -> user
    end
    case Api.Repo.delete(user) do
        {:ok, _} -> json conn, %{id: user.id}
        {:error, changeset} ->
            conn
            |> put_status(:internal_server_error)
            |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end

  swagger_path :login do
    get "/user/login"
    summary "Login"
    description "Authentication"
    parameter :query, :email, :string, "user's email", required: true
    parameter :query, :password, :string, "user's password", required: true
    response 200, "Ok", Schema.ref(:Users)
    tag "users"
  end

  def login(conn, %{"email" => email, "password" => password}) do
    # Check if user exist
    case User
      |> Api.Repo.get_by(%{email: email})
      |> Api.Repo.preload([:roles, {:roles, :permissions}])
      do
        nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "Invalid email or password"})
        user ->
          case Bcrypt.verify_pass(password, user.password_hash) do
            false ->
                conn
                |> put_status(:not_found)
                |> json(%{error: "Invalid email or password"})
            true ->
              case Bcrypt.verify_pass(password, user.password_hash) do
                false ->
                    conn
                    |> put_status(:not_found)
                    |> json(%{error: "Invalid email or password"})
                true ->
                  user = Jason.encode!(user)
                  user = Jason.decode!(user)
                  json(conn, %{
                      user: user,
                      token: %{
                          type: "Bearer",
                          value: ApiWeb.Token.generate_and_sign!(user)
                      }
                  })
              end
          end
      end
  end
  def swagger_definitions do
     %{
       User: swagger_schema do
         title "User"
         description "A user"
         properties do
           id :integer, "The ID of the user"
           username :utc_datetime, "The username", required: true
           email :string, "The email of the user", required: true
           password :string, "user's password", required: true
         end
         example %{
           username: "username",
           email: "email@gmail.com"
         }
       end
     }
  end
end
