defmodule ApiWeb.WorkingTimeController do
  use ApiWeb, :controller
  use Timex
  use PhoenixSwagger

  import Ecto.Query, only: [from: 2]

  swagger_path :getByUserId do
    get "/workingtimes"
    summary "Get working times"
    description "Get working periods, filtering by user ID"
    parameter :query, :id, :integer, "user id", required: true
    response 200, "", Schema.ref(:WorkingTimes)
    tag "workingtimes"
  end

  def getByUserId(conn, %{"userId" => userId}) do
    case Integer.parse(userId) do
      {userId, ""} ->
        q = from m in WorkingTime, where: m.user == ^userId
        case Api.Repo.all(q) do
          nil ->
            json conn, []
          res ->
            json conn, res
        end
      _ ->
        conn
        |> put_status(:not_acceptable)
        |> json(%{error: "Invalid userId"})
    end
  end

  swagger_path :getByWorkingTimeId do
    get "/workingtimes"
    summary "Get working time"
    description "Get working period, filtering by workingtime ID"
    parameter :query, :id, :integer, "workingtime id", required: true
    response 200, "", Schema.ref(:WorkingTimes)
    tag "workingtimes"
  end

  def getByWorkingTimeId(conn, %{"workingtimeID" => workingtimeID}) do
    workingtimeID = case Integer.parse(workingtimeID) do
      {parsed, ""} -> parsed
      _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "Invalid workingtimeID"})
    end
    case Api.Repo.get(WorkingTime, workingtimeID) do
        nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "Working Time not found"})
            workingtime -> json conn, workingtime
    end
  end

  swagger_path :create do
    get "/workingtimes"
    summary "Create a working time"
    description "Create a working time"
    parameter :query, :start, :utc_datetime, "workingtime start", required: true
    parameter :query, :endDate, :utc_datetime, "workingtime end", required: true
    response 200, "", Schema.ref(:WorkingTimes)
    tag "workingtimes"
  end

  def create(conn, params) do
    workingtime = WorkingTime.changeset(%WorkingTime{}, params)
    case Api.Repo.insert(workingtime) do
      {:ok, workingtime} -> json conn, workingtime
      {:error, changeset} ->
        conn
        |> put_status(:not_acceptable)
        |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end

  swagger_path :update do
    get "/workingtimes"
    summary "Update a working time"
    description "Update a working time"
    parameter :query, :start, :utc_datetime, "workingtime start", required: true
    parameter :query, :endDate, :utc_datetime, "workingtime end", required: true
    response 200, "", Schema.ref(:WorkingTimes)
    tag "workingtimes"
  end

  def update(conn, params) do
    workingtime = case Api.Repo.get(WorkingTime, params["id"]) do
      nil ->
          conn
          |> put_status(:not_found)
          |> json(%{error: "Working Time not found"})
      user -> user
    end

    if Map.has_key?(params, "start") do
      {:ok, startDate} = Timex.parse(params["start"],  "%Y-%m-%d %H:%M:%S", :strftime)
      Map.replace(params, "start", startDate)
    end
    if Map.has_key?(params, "end") do
      {:ok, endDate} = Timex.parse(params["end"],  "%Y-%m-%d %H:%M:%S", :strftime)
      Map.replace(params, "end", endDate)
    end

    workingtime = WorkingTime.changeset(workingtime, params)
    case Api.Repo.update(workingtime) do
        {:ok, workingtime} -> json conn, workingtime
        {:error, changeset} ->
            conn
            |> put_status(:not_acceptable)
            |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end

  swagger_path :delete do
    get "/workingtimes"
    summary "Delete a working time"
    description "Delete a working time, filtering by workingtime ID"
    parameter :query, :id, :integer, "workingtime ID", required: true
    response 200, "", Schema.ref(:WorkingTimes)
    tag "workingtimes"
  end

  def delete(conn, %{"id" => id}) do
    workingtime = case Api.Repo.get(WorkingTime, id) do
        nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "Working Time not found"})
        user -> user
    end
    case Api.Repo.delete(workingtime) do
        {:ok, _} -> json conn, %{id: workingtime.id}
        {:error, changeset} ->
            conn
            |> put_status(:internal_server_error)
            |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
    end
  end

  def swagger_definitions do
    %{
      WorkingTime: swagger_schema do
        title "WorkingTime"
        description "A user's work period"
        properties do
          id :integer, "The ID of the workingtime"
          start :utc_datetime, "The start date of the working time", required: true
          endDate :utc_datetime, "End date of the working time", required: true
        end
        example %{
          start: "2017-03-21T14:00:00Z",
          endDate: "2017-04-21T14:00:00Z"
        }
      end
    }
  end

end
