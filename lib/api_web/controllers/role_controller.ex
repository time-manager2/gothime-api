defmodule ApiWeb.RoleController do
  use ApiWeb, :controller
  use PhoenixSwagger

  swagger_path :get_roles do
    get "/roles"
    summary "Get roles"
    description "Get all the roles existing"
    response 200, "", Schema.ref(:Roles)
    tag "roles"
  end

  def get_roles(conn, _) do
    roles = Role.get_all()
    json conn, roles
  end

  swagger_path :assign_to_user do
    get "/roles"
    summary "Assign"
    description "Assign a role to a user"
    parameter :query, :roleID, :integer, "Role's ID", required: true
    parameter :query, :userID, :integer, "User's ID", required: true
    response 200, "", Schema.ref(:User)
    tag "roles"
  end

  def assign_to_user(conn, %{"roleId" => roleId, "userId" => userId}) do
    roleId = case Integer.parse(roleId) do
      {parsed, ""} -> parsed
      _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "Invalid roleId"})
    end
    userIdInt = case Integer.parse(userId) do
      {parsed, ""} -> parsed
      _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "Invalid userId"})
    end
    User.add_role(%{id: userIdInt}, %{id: roleId})
    case User
            |> Api.Repo.get(userId)
            |> Api.Repo.preload([:roles, {:roles, :permissions}])
        do
        nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "User not found"})
        user -> json conn, user
      end
  end

  def swagger_definitions do
    %{
      User: swagger_schema do
        title "User"
        description "A user"
        properties do
          name :string, "Name of the role", required: true
        end
        example %{
          name: "manager"
        }
      end
    }
  end
end
