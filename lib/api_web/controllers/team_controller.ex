defmodule ApiWeb.TeamController do
  use ApiWeb, :controller
  use PhoenixSwagger

  import Ecto.Query, only: [from: 2]



    def create(conn, params) do
        team = User.changeset(%User{}, params)
        case Api.Repo.insert(team) do
          {:ok, team} -> json conn, team
          {:error, changeset} ->
            conn
            |> put_status(:not_acceptable)
            |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
        end
    end

    def getById(conn, %{"teamId" => teamId}) do
      teamId = case Integer.parse(teamId) do
          {parsed, ""} -> parsed
          _ ->
              conn
              |> put_status(:not_acceptable)
              |> json(%{error: "Invalid teamId"})
      end
      case Api.Repo.get(Team, teamId) do
          nil ->
              conn
              |> put_status(:not_found)
              |> json(%{error: "Team not found"})
          team -> json conn, team
      end
    end

    def getByUserId(conn, %{"userId" => userId}) do
      userId = case Integer.parse(userId) do
          {parsed, ""} -> parsed
          _ ->
              conn
              |> put_status(:not_acceptable)
              |> json(%{error: "Invalid userId"})
      end
      user = case Api.Repo.get(User, userId) do
          nil ->
              conn
              |> put_status(:not_found)
              |> json(%{error: "User not found"})
          user -> user
      end
      case Api.Repo.all from t in Team,
      where: t.userId == ^user.id,
      preload: [:user] do
        nil ->
          conn
          |> put_status(:not_found)
          |> json(%{error: "Team not found"})
        team -> json conn, team
      end
    end

    def getByPeriod(conn, params) do
      teamId = Team.changeset(%Team{}, params)
      paramStart = case Integer.parse(params["start"]) do
        {parsed, ""} -> parsed
        _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "invalid start"})
      end
      paramEnd = case Integer.parse(params["endDate"]) do
        {parsed, ""} -> parsed
        _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "invalid end date"})
      end

      teamId = case Integer.parse(teamId) do
        {parsed, ""} -> parsed
        _ ->
            conn
            |> put_status(:not_acceptable)
            |> json(%{error: "Invalid teamId"})
      end

      _workingtimes = case Api.Repo.all from t in Team,
        join: w in Workingtime, on: [id: w.team],
        where: t.id == ^teamId
        and ^paramStart > w.start
        and ^paramEnd < w.endDate,
        preload: [:workingtime] do
          nil ->
            conn
            |> put_status(:not_found)
            |> json(%{error: "No workingtime found"})
          workingtimes -> json conn, workingtimes
      end
    end

    def update(conn, params) do
      teamId = case Integer.parse(params["teamId"]) do
        {parsed, ""} -> parsed
        _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "Invalid teamId"})
      end
      team = case Api.Repo.get(Team, teamId) do
        nil ->
          conn
          |> put_status(:not_found)
          |> json(%{error: "Team not found"})
          team -> team
      end
      team = Team.changeset(team, params)
      case Api.Repo.update(team) do
        {:ok, team} -> json conn, team
        {:error, changeset} ->
          conn
          |> put_status(:not_acceptable)
          |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
      end
    end


    def delete(conn, %{"teamID" => teamId}) do
      teamId = case Integer.parse(teamId) do
        {parsed, ""} -> parsed
        _ ->
          conn
          |> put_status(:not_acceptable)
          |> json(%{error: "Invalid teamId"})
      end
      team = case Api.Repo.get(Team, teamId) do
        nil ->
          conn
          |> put_status(:not_found)
          |> json(%{error: "Team not found"})
          team -> team
      end
      case Api.Repo.delete(team) do
        {:ok, _} -> json conn, %{id: team.id}
          {:error, changeset} ->
            conn
            |> put_status(:internal_server_error)
            |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
      end
    end

end
