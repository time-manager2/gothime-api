defmodule ApiWeb.ClockController do
    use ApiWeb, :controller
    use Timex
    use PhoenixSwagger

    @spec create(Plug.Conn.t(), %{optional(:__struct__) => none, optional(atom | binary) => any}) ::
            Plug.Conn.t()
    def create(conn, params) do
      clock = Clock.changeset(%Clock{}, params)

      case Api.Repo.insert(clock) do
          {:ok, time} -> json conn, time
          {:error, changeset} ->
              conn
              |> put_status(:not_acceptable)
              |> render(ApiWeb.ErrorView, "error.json", %{changeset: changeset})
      end
    end

    swagger_path :getByUserId do
      get "/api/clocks/{userId}"
      description "Get user's clock"
      response 200, "Ok", Schema.ref(:Clocks)
    end

    def getByUserId(conn, %{"userId" => userId}) do
      userId = case Integer.parse(userId) do
          {parsed, ""} -> parsed
          _ ->
              conn
              |> put_status(:not_acceptable)
              |> json(%{error: "Invalid userId"})
      end
      case Api.Repo.get(Clock, userId) do
          nil ->
              conn
              |> put_status(:not_found)
              |> json(%{error: "User not found"})
          clock -> json conn, clock
      end
  end

  def swagger_definitions do
    %{
      Clock: swagger_schema do
        title "Clock"
        description "A user have clocked in or clocked out"
        properties do
          id :integer, "The ID of the clock"
          time :string, "The clock recorded", required: true, format: "ISO-8601"
          status :integer, "True when clock in", required: true
        end
        example %{
          time: "2017-03-21T14:00:00Z",
          status: 1
        }
      end
    }
  end
end
