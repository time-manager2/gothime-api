defmodule User do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  @derive {Jason.Encoder, only: [:id, :email, :username, :roles]}
  schema "users" do
    field :username, :string
    field :email, :string
    field :password_hash, :string
    many_to_many :roles, Role, join_through: "users_roles"
    timestamps()
  end

  def changeset(user, params \\ %{}) do
    user
    |> cast(params, [:username, :email, :password_hash])
    |> validate_format(:email, ~r/.{1,}@.{1,}\..{1,}/)
    |> validate_required([:username, :email, :password_hash])
    |> unique_constraint(:email)
  end

  def add_role(user, role) do
    case UsersRoles.create(user, role) do
      {:ok, _} ->
        User
          |> Api.Repo.get(user.id)
          |> Api.Repo.preload([:roles, {:roles, :permissions}])
      {:error, _} -> user
    end
  end

  def getByRoleId(roleId) do
    query =
    from u in User,
      join: ur in UsersRoles,
      on: ur.user_id == u.id,
      where: ur.role_id == ^roleId
    user = Api.Repo.all(query)
    |> Api.Repo.preload([:roles, {:roles, :permissions}])
  end

  def get_managers() do
    getByRoleId(2)
  end

  def get_employees() do
    getByRoleId(3)
  end
end
