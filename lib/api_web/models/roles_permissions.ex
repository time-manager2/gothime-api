defmodule RolesPermissions do
  use Ecto.Schema

  schema "roles_permissions" do
    belongs_to(:role, Role, primary_key: true)
    belongs_to(:permission, Permission, primary_key: true)

    timestamps()
  end


  def create(%{:id => role_id}, %{:id => permission_id}) do
    role_permission = %RolesPermissions{
      role_id: role_id,
      permission_id: permission_id
    }
    Api.Repo.insert(role_permission)
  end
end
