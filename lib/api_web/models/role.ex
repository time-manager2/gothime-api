defmodule Role do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :name, :display_name, :permissions]}
  schema "roles" do
    field :name, :string
    field :display_name, :string
    many_to_many :permissions, Permission, join_through: "roles_permissions"
    timestamps()
  end

  def changeset(role, params \\ %{}) do
    role
    |> cast(params, [:name, :display_name])
    |> validate_required([:name, :display_name])
    |> unique_constraint(:name)
  end

  def create(%{
    name: _,
    display_name: _
  } = params) do
    role = changeset(%Role{}, params)
    Api.Repo.insert(role)
  end

  def add_permission(role, permission) do
    RolesPermissions.create(role, permission)
  end

  def get_by_name(name) do
    role = Api.Repo.get_by(Role, %{name: name})
    role
    |> Api.Repo.preload(:permissions)
  end

  def get_all() do
    role = Api.Repo.all(Role)
    role
    |> Api.Repo.preload(:permissions)
  end
end
