defmodule UsersRoles do
  use Ecto.Schema

  schema "users_roles" do
    belongs_to(:user, User, primary_key: true)
    belongs_to(:role, Role, primary_key: true)

    timestamps()
  end


  def create(%{:id => user_id}, %{:id => role_id}) do
    user_role = %UsersRoles{
      user_id: user_id,
      role_id: role_id
    }
    Api.Repo.insert(user_role)
  end
end
