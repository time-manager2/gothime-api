defmodule Permission do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :name, :display_name]}
  schema "permissions" do
    field :name, :string
    field :display_name, :string
    timestamps()
  end

  def changeset(permission, params \\ %{}) do
    permission
    |> cast(params, [:name, :display_name])
    |> validate_required([:name, :display_name])
    |> unique_constraint(:name)
  end

  def create(%{
    name: _,
    display_name: _
  } = params) do
    permission = changeset(%Permission{}, params)
    Api.Repo.insert(permission)
  end

  def get_by_name(name) do
    Api.Repo.get_by(Permission, %{name: name})
  end
end
