defmodule Clock do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:time, :status]}
  schema "clocks" do
    field :time, :utc_datetime
    field :status, :boolean
    belongs_to :user, User
  end

  def changeset(workingTime, params \\ %{}) do
    workingTime
    |> cast(params, [:time, :status, :user])
    |> validate_required([:time, :status, :user])
  end
end
