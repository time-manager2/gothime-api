defmodule Team do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:userID]}
  schema "teams" do
    field :userID?, :integer
    has_many :user, User
    has_many :workingtime, WorkingTime
  end

  def changeset(team, params \\ %{}) do
    team
    |> cast(params, [:userID, :user, :workingtime])
    |> validate_required([:user, :workingtime])
  end
end
