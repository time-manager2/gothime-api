defmodule WorkingTime do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :start, :end, :user]}
  schema "workingtimes" do
    field :start, :naive_datetime
    field :end, :naive_datetime
    field :user, :integer
    timestamps()
  end

  def changeset(workingTime, params \\ %{}) do
    workingTime
    |> cast(params, [:start, :end, :user])
    |> validate_required([:start, :end, :user])
    end
end
