defmodule ApiWeb.Plugs.JWT do
  import Plug.Conn

  def init(default), do: default

  defp check_permissions(user, permissions_allowed) do
    case permissions_allowed do
      nil -> true
      [] -> true
      permissions_allowed ->
        case Enum.find(user.roles, fn role ->
          Enum.find(role.permissions, fn permission ->
            Enum.member?(permissions_allowed, permission.name)
          end)
        end) do
          nil -> false
          _ -> true
        end
    end
  end
  def call(conn, params) do
    token = Enum.find(conn.req_headers, fn header ->
      elem(header, 0) == "authorization"
    end)
    case token do
      nil ->
        conn
        |> resp(:forbidden, Jason.encode!(%{
          error: "Authorization not found"
        }))
        |> halt()
      token ->
        case String.split(elem(token, 1)) do
          ["Bearer", value] ->
            case ApiWeb.Token.verify(value) do
              {:ok, %{"id" => id}} ->
                case User
                  |> Api.Repo.get(id)
                  |> Api.Repo.preload([:roles, {:roles, :permissions}])
                  do
                  nil ->
                    conn
                    |> resp(:not_found, Jason.encode!(%{
                      error: "User not found"
                    }))
                    |> halt()
                  user ->
                    case check_permissions(user, params[:permissions]) do
                      true ->
                        assign(conn, :user_logged, user)
                      false ->
                        conn
                        |> resp(:forbidden, Jason.encode!(%{
                          error: "Invalid permissions"
                        }))
                        |> halt()
                    end
                end
              {:error, content} ->
                conn
                |> resp(:forbidden, Jason.encode!(%{
                  error: "Invalid token",
                  content: content
                }))
                |> halt()
            end
          _ ->
            conn
            |> resp(:forbidden, Jason.encode!(%{
              error: "Only Bearer token accepted"
            }))
            |> halt()
        end
    end
  end
end
