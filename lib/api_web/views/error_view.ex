defmodule ApiWeb.ErrorView do
  use ApiWeb, :view
  def translate_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, &translate_error/1)
  end

  def render("error.json", %{changeset: changeset}) do
    %{
      error: "Invalid object",
      content: translate_errors(changeset)
    }
  end

  def template_not_found(template, _) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end
end
